﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;

public class CubeMovement : MonoBehaviour {

    Script script;
    Rigidbody rb;

    public void Awake()
    {
        script = new Script();
        
    }

    // Use this for initialization
    void Start()
    {
        string scriptCode = @"    
			-- defines the update function
			function moveLeft(x)
				obj.Move(x)
			end
            function moveRight(x)
                obj.Move(x)
            end
            function jump(x)
                obj.Jump(x)
            end";


        UserData.RegisterType<CubeMovement>();

        DynValue obj = UserData.Create(this);

        script.Globals.Set("obj", obj);

        script.DoString(scriptCode);
    }

    public void Move(float x)
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.AddForce(x, 0, 0, ForceMode.Impulse);
    }

    public void Jump(float x) {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.AddForce(0, x, 0, ForceMode.VelocityChange);
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            script.Call(script.Globals["moveRight"], Time.deltaTime* 10);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            script.Call(script.Globals["moveLeft"], Time.deltaTime* -10);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            script.Call(script.Globals["jump"], Time.deltaTime* 200);
        }

    }
}
