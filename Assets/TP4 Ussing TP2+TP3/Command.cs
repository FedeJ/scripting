﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Command
{
    public enum State { InProgress, Finished, Error }

    public CommandManager manager;

    public virtual void Init() { }
    public abstract State Execute();
}
