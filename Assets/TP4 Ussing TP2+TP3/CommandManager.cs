﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandManager : MonoBehaviour {

    public Command actualCommand;
    public Queue<Command> commands = new Queue<Command>();

    public void AddCommand(Command command)
    {
        command.manager = this;
        commands.Enqueue(command);
    }

    void Update()
    {
        if (actualCommand != null)
        {
            Command.State state = actualCommand.Execute();

            if (state == Command.State.Finished)
            {
                actualCommand = null;
            }

            if (state == Command.State.Error)
            {
                actualCommand = null;
                commands.Clear();
            }
        }
        else if (commands.Count > 0)
        {
            actualCommand = commands.Dequeue();
            actualCommand.Init();
        }
    }
}
