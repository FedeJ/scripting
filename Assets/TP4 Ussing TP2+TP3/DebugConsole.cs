﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugConsole : MonoBehaviour
{

    public delegate string CommandDlg(params string[] args);
    private static DebugConsole instance = null;

    private Dictionary<string, CommandDlg> commands = new Dictionary<string, CommandDlg>();

    public Parser parser;

    private void Awake()
    {
        parser = new Parser();
    }

    public static DebugConsole Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<DebugConsole>();
            }
            return instance;
        }
    }


    public void RegisterCommand(string commandName, CommandDlg command)
    {
        commandName = commandName.ToLower();
        commands.Add(commandName, command);
    }

    public string ExecuteCommand(string str)
    {
        string log = "Input: " + str + "\n";

        if (!parser.Parse(str))
            log+= "No command called \"" + str + "\" has been found";

        if (commands.ContainsKey(parser.GetCommandData().CommandName.ToLower()))
            log+= commands[parser.GetCommandData().CommandName.ToLower()](parser.GetCommandData().Args.ToArray());
        else log += "SyntaxError";
        return log;
        
        /*string[] lexemes = str.Split(' ');
        string cmd = lexemes[0].ToLower();

        if (commands.ContainsKey(cmd))
        {
            string log = "Input: " + cmd + "\n";
            string[] args = null;

            if (lexemes.Length > 1)
            {
                args = new string[lexemes.Length - 1];
                for (int i = 1; i < lexemes.Length; i++)
                {
                    args[i - 1] = lexemes[i];
                }

                log += commands[cmd](args);
            }
            else //In case of command without arguments
            {
                log += commands[cmd](args);
                return log;
            }
            return log;
        }
        return "No command called \"" + cmd + "\" has been found";
        */
    }
}
