﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandData
{
    public string CommandName;
    public List<string> Args;
}

public class Parser
{
    enum State
    {
        CommandName,
        Arguments
    }

    public Tokenizer tokenizer = new Tokenizer();
    CommandData command = new CommandData();
    State state = State.CommandName;

    public CommandData GetCommandData() {
        if (command.CommandName != null)
        {
            return command;
        }
        return null;
    }

    void Reset()
    {
        command.CommandName = null;
        command.Args = new List<string>();
        state = State.CommandName;
    }

    public bool Parse(string str)
    {
        Reset();
        tokenizer.Start(str);

        var currentToken = tokenizer.GetNextToken();

        if (currentToken.Type == Tokenizer.TokenType.Unknown)
        {
            return false;
        }

        while (currentToken.Type != Tokenizer.TokenType.Fin && currentToken.Type!= Tokenizer.TokenType.Unknown)
        {
            switch (state)
            {
                case State.CommandName:
                    if (currentToken.Type != Tokenizer.TokenType.Ident)
                        return false;

                    command.CommandName = currentToken.Lexeme;
                    currentToken = tokenizer.GetNextToken();

                    if (currentToken.Type == Tokenizer.TokenType.OpenParent)
                    {
                        state = State.Arguments;
                        currentToken = tokenizer.GetNextToken();
                    } else if (currentToken.Type == Tokenizer.TokenType.Equals)
                    {
                        state = State.Arguments;
                        command.Args.Add(command.CommandName);
                        command.CommandName = "Var";
                    }
                    else if (currentToken.Type == Tokenizer.TokenType.Fin)
                        return true;
                    else
                        continue;
                    break;
                case State.Arguments:
                    if (currentToken.Type == Tokenizer.TokenType.Number || currentToken.Type == Tokenizer.TokenType.Ident || currentToken.Type == Tokenizer.TokenType.String)
                    {
                        command.Args.Add(currentToken.Lexeme);
                        currentToken = tokenizer.GetNextToken();

                        if (currentToken.Type == Tokenizer.TokenType.Comma)
                            currentToken = tokenizer.GetNextToken();
                        else if (currentToken.Type == Tokenizer.TokenType.CloseParent)
                        {
                            currentToken = tokenizer.GetNextToken();
                            if (currentToken.Type != Tokenizer.TokenType.Fin)
                                return false;
                            else
                                return true;
                        }
                        else
                            return false;
                    }
                    else if (currentToken.Type == Tokenizer.TokenType.CloseParent)
                    {
                        return true;
                    }
                    
                    else if (currentToken.Type == Tokenizer.TokenType.Equals)
                    {
                        command.Args.Add(currentToken.Lexeme);
                        return true;
                    }
                    break;
                default:
                    break;
            }
        }

        return false;
    }
}
