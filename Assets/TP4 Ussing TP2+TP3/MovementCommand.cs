﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementCommand : Command {

    public int cantToMove;
    public GameObject gameObject;

    private Vector3 whereMove;
    private Rigidbody rb;

    public MovementCommand(int moveCant, GameObject go) {
        this.cantToMove = moveCant;
        gameObject = go;
    }

    public override void Init()
    {
        whereMove = gameObject.transform.position + new Vector3 (cantToMove ,0,0);
        rb = gameObject.GetComponent<Rigidbody>();
    }

    public override State Execute()
    {
        if (!rb) return State.Error;

        rb.transform.position = Vector3.MoveTowards(rb.transform.position, whereMove, Time.deltaTime * 10);

        if (Vector3.Distance(rb.transform.position, whereMove) <= 0.1f)
        {
            return State.Finished;
        }
        else return State.InProgress;
       
    }
}
