﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugConsoleView : MonoBehaviour
{

    public Text ConsoleTxt;
    public InputField InputConsole;

    private void Start()
    {
        InputConsole.onEndEdit.AddListener(OnEndEdit);
        DebugConsole.Instance.RegisterCommand("Help", HelpCommand);
        DebugConsole.Instance.RegisterCommand("Var", VarCommand);
        DebugConsole.Instance.RegisterCommand("Log", LogCommand);
        DebugConsole.Instance.RegisterCommand("CreateObject", CreateObjectCommand);
        DebugConsole.Instance.RegisterCommand("Move", MoveCommand);
        DebugConsole.Instance.RegisterCommand("Pause", PauseCommand);
        DebugConsole.Instance.RegisterCommand("Play", PlayCommand);
    }

    void OnEndEdit(string val)
    {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            ConsoleTxt.text += "\n" + DebugConsole.Instance.ExecuteCommand(val);
            InputConsole.text = "";
        }
    }

    public string HelpCommand(params string[] args)
    {
        return "\nLog([text]) -- Shows text in Console\n" +
            "CreateObject([objectName]) -- Load object to Scene looking in Resourse Folder\n" +
            "Move([objectName],[lateralValue]) -- Shows text in Console\n" +
            "Var([VarName],[Item]) -- Assign to VarName the item given\n" +
            "[Ident]=[Var] -- Assign to Ident the item Var\n" +
            "Pause() -- Pause the Game\n" +
            "Play() -- UnPause the Game\n" +
            "Help() -- This command\n";
    }

    public string LogCommand(params string[] args)
    {
        var log = "";
        for (int i = 0; i < args.Length; i++)
        {
            log += args[i] + " ";
        }
        return log;
    }

    public string VarCommand(params string[] args) {

        if (args.Length == 2)
        {
            DebugConsole.Instance.parser.tokenizer.vars.Add(args[0], args[1]);
            return "variable: \"" + args[0] + "\" have the item \"" + args[1] + "\" assigned";
        }
        else
            return "Arguments Invalid: use \"Var [VarName] [Item]\"";
    }

    public string CreateObjectCommand(params string[] args)
    {
        var go = (Resources.Load(args[0]));

        if (!go) return "No object called \"" + args[0] + "\" exists.";

        Instantiate(go);
        return "object \"" + args[0] + "\" created";
    }

    public string MoveCommand(params string[] args)
    {
        if (args.Length == 2)
        {
            GameObject go = GameObject.Find(args[0]);

            if (!go) return "No object called \"" + args[0] + "\" founded";

            var commandManager = go.GetComponent<CommandManager>();

            if (!commandManager) return "object called \"" + args[0] + "\" can't be moved";

            int val = 0;
            if (!int.TryParse(args[1], out val))
                return "IntegerValue Invalid: use \"Move [ObjectName] [IntegerValue]\"";

            Command command = new MovementCommand(val, go);
            commandManager.AddCommand(command);

            return "Moving " + args[0] + " to " + args[1] + " values"; ;
        }
        else
            return "Arguments Invalid: use \"Move [ObjectName] [IntegerValue]\"";
    }

    public string PauseCommand(params string[] args)
    {
        Time.timeScale = 0;
        return "Game Paused";
    }
    public string PlayCommand(params string[] args)
    {
        Time.timeScale = 1;
        return "Game UnPaused";
    }
}
