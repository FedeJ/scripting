﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tokenizer
{
    public Dictionary<string, string> vars = new Dictionary<string, string>();

    public enum TokenType
    {
        Ident,
        Equals,
        OpenParent,
        CloseParent,
        Comma,
        String,
        Number,
        Fin,
        Unknown,
        Empty
    }

    public struct Token
    {
        public string Lexeme;
        public TokenType Type;
    }

    private string actualString;
    private int idStart = 0;
    private int idEnd = 0;
    private Token token = new Token();

    private void Restart()
    {
        actualString = null;
        idStart = 0;
        idEnd = 0;
    }

    public void Start(string str)
    {
        Restart();
        actualString = RemoveSpaces(str);
    }

    public Token GetNextToken()
    {
        idStart = idEnd;

        if (string.IsNullOrEmpty(actualString))
        {
            token.Lexeme = "";
            token.Type = TokenType.Empty;
        }
        else if (idStart >= actualString.Length)
        {
            token.Lexeme = "";
            token.Type = TokenType.Fin;
        }
        else
        {
            if (char.IsLetter(actualString[idStart]))
            {
                token.Type = TokenType.Ident;
                token.Lexeme = GetLexemeFromString(actualString);
            }
            else if (char.IsDigit(actualString[idStart]) || actualString[idStart] == '-')
            {
                token.Type = TokenType.Number;
                token.Lexeme = GetLexemeFromString(actualString);
            }
            else if (actualString[idStart] == '(')
            {
                token.Lexeme = actualString[idStart].ToString();
                token.Type = TokenType.OpenParent;
                idStart++;
                idEnd++;
            }
            else if (actualString[idStart] == ')')
            {
                token.Lexeme = actualString[idStart].ToString();
                token.Type = TokenType.CloseParent;
                idStart++;
                idEnd++;
            }
            else if (actualString[idStart] == ',')
            {
                token.Lexeme = actualString[idStart].ToString();
                token.Type = TokenType.Comma;
                idStart++;
                idEnd++;
            }
            else if (actualString[idStart] == '"')
            {
                token.Lexeme = GetStringFromQuotMarks(actualString);
                token.Type = TokenType.String;
                idStart++;
                idEnd++;
            }
            else if (actualString[idStart] == '=')
            {
                token.Lexeme = AssingValueToVar(actualString);
                token.Type = TokenType.Equals;
                idStart++;
                idEnd++;
            }
            else
            {
                token.Lexeme = null;
                token.Type = TokenType.Unknown;
            }
        }
        return token;
    }

    private string AssingValueToVar(string str)
    {
        string lexeme = "";
        idEnd = idStart;

        if (str[idEnd] == '=')
            idEnd++;

        while (idEnd != str.Length)
        {
            lexeme += str[idEnd++];
        }
        return lexeme;
    }

    //Takes the string between the Quot Marks, Exclusive for the Quot Marks
    private string GetStringFromQuotMarks(string str)
    {
        string lexeme = "";

        idEnd = idStart;


        if (str[idEnd] == '"')
            idEnd++;

        while (idEnd != str.Length)
        {
            lexeme += str[idEnd++];

            if (idEnd >= str.Length || str[idEnd] == '"')
            {
                break;
            }
        }

        return lexeme;
    }

    private string GetLexemeFromString(string str)
    {
        string lexeme = "";
        idEnd = idStart;

        while (idEnd != str.Length)
        {
            lexeme += str[idEnd++];
            if (idEnd >= str.Length || IsSeparator(str[idEnd]))
            {
                break;
            }
        }
        //Looks if the dictionary haves a variable with the lexeme name
        if (vars.ContainsKey(lexeme))
        {
            lexeme = vars[lexeme];
            
            //In case is a string with Commas
            if (lexeme[0] == '"' && lexeme[lexeme.Length - 1] == '"')
            {
                lexeme = lexeme.Split('"')[1];
                Debug.Log(lexeme);
            }
        }
        return lexeme;
    }

    //If its not a letter or a digit, then is a Separator
    private bool IsSeparator(char ch)
    {
        return !char.IsLetterOrDigit(ch);
    }

    //Remove spaces, except the spaces between this symbols: "text text"
    private string RemoveSpaces(string str)
    {
        bool betweenComma = false;
        string resultString = "";

        for (int i = 0; i < str.Length; i++)
        {
            var ch = str[i];
            if (char.IsWhiteSpace(ch) && !betweenComma)
            {
                continue;
            }
            else
            if (ch == '"')
            {
                betweenComma = !betweenComma;
            }
            resultString += ch;
        }
        return resultString;
    }
}
