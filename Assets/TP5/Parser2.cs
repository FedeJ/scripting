﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parser2
{
    enum State
    {
        Ident,
        Arguments
    }

    List<MyInstruction> instructions = new List<MyInstruction>();
    Dictionary<string, string> variables = new Dictionary<string, string>();


    Tokenizer2 tokenizer = new Tokenizer2();

    State state = State.Ident;

    Tables tables;

    public Parser2(Tables tables)
    {
        this.tables = tables;
    }

    public void Reset()
    {
        tables.Clear();
        state = State.Ident;
        instructions.Clear();
    }

    public bool Parse(string str, out List<MyInstruction> instructions, out Dictionary<string, string> variables)
    {
        Reset();
        tokenizer.Start(str);

        MyInstruction currentInstruction = new MyInstruction();

        instructions = this.instructions;
        variables = this.variables;

        Tokenizer2.Token2 currentToken = tokenizer.GetNextToken();

        if (currentToken.Type == Tokenizer2.TokenType.Empty)
        {
            return false;
        }

        while (currentToken.Type != Tokenizer2.TokenType.EOF && currentToken.Type != Tokenizer2.TokenType.Unknown)
        {
            string ident = currentToken.Lexeme;

            var posibleVar = currentToken;
            currentToken = tokenizer.GetNextToken(); // skip to next token

            if (currentToken.Type == Tokenizer2.TokenType.Colon) // If it's a colon, then the ident is a Label
            {
                if (!tables.AddLabel(ident, instructions.Count)) // Couldn't add label, probably already exists
                    return false;
                currentToken = tokenizer.GetNextToken(); // skip to next token
            }
            else if (currentToken.Type == Tokenizer2.TokenType.Var)
            {
                if (!variables.ContainsKey(posibleVar.Lexeme))
                    variables.Add(posibleVar.Lexeme, currentToken.Lexeme);
                else
                    variables[posibleVar.Lexeme] = currentToken.Lexeme;
            }
        }

        state = State.Ident;
        tokenizer.Start(str);
        currentToken = tokenizer.GetNextToken();

        while (currentToken.Type != Tokenizer2.TokenType.EOF && currentToken.Type != Tokenizer2.TokenType.Unknown)
        {
            switch (state)
            {
                case State.Ident:
                    if (currentToken.Type != Tokenizer2.TokenType.Ident)
                        return false;

                    string ident = currentToken.Lexeme;

                    currentToken = tokenizer.GetNextToken(); // skip to next token

                    if (currentToken.Type == Tokenizer2.TokenType.Colon) // If it's a colon, then the ident is a Label
                    {
                        //This time there is no need to add labels
                        currentInstruction = new MyInstruction();
                        currentInstruction.OpCode = OpCodes.NOP;
                        instructions.Add(currentInstruction); // We add a NOP instruction just to be sure 

                        currentToken = tokenizer.GetNextToken(); // skip to next token
                    }
                    else if (currentToken.Type == Tokenizer2.TokenType.Var)
                    {
                        //in this case, we do nothing, just pass
                        currentToken = tokenizer.GetNextToken();
                    }
                    else // If it isn't, then probably it's an instruction
                    {
                        if (variables.ContainsKey(ident))
                        {
                            ident = variables[ident];
                        }

                        int opCode = 0;

                        if (!tables.GetInstrLookUp(ident, out opCode))
                            return false;

                        currentInstruction = new MyInstruction();

                        currentInstruction.OpCode = opCode;

                        if (currentToken.Type == Tokenizer2.TokenType.OpenParent)
                        {
                            state = State.Arguments;
                            currentToken = tokenizer.GetNextToken(); // Skip parenthesis

                            currentInstruction.Arguments = new List<string>();
                        }
                        else if (currentToken.Type != Tokenizer2.TokenType.EOL && currentToken.Type != Tokenizer2.TokenType.EOF)
                            return false;
                    }

                    break;

                case State.Arguments:
                    if (currentToken.Type == Tokenizer2.TokenType.Number || currentToken.Type == Tokenizer2.TokenType.String)
                    {
                        currentInstruction.Arguments.Add(currentToken.Lexeme);

                        currentToken = tokenizer.GetNextToken();

                        if (currentToken.Type == Tokenizer2.TokenType.Comma)
                            currentToken = tokenizer.GetNextToken(); // skip comma
                        else if (currentToken.Type == Tokenizer2.TokenType.CloseParent)
                        {
                            currentToken = tokenizer.GetNextToken();

                            if (currentToken.Type != Tokenizer2.TokenType.EOL && currentToken.Type != Tokenizer2.TokenType.EOF)
                                return false;

                            instructions.Add(currentInstruction);

                            state = State.Ident;
                        }
                        else
                            return false; // Syntax error! 
                    }
                    else if (currentToken.Type == Tokenizer2.TokenType.Ident) // If there's an identifier, then maybe is a GoTo
                    {
                        if (variables.ContainsKey(currentToken.Lexeme))
                        {//is a variable, then takes the assigned value
                            currentInstruction.Arguments.Add(variables[currentToken.Lexeme]);
                            currentToken = tokenizer.GetNextToken();
                            if (currentToken.Type == Tokenizer2.TokenType.Comma)
                                currentToken = tokenizer.GetNextToken(); // skip comma
                            else if (currentToken.Type == Tokenizer2.TokenType.CloseParent)
                            {
                                currentToken = tokenizer.GetNextToken();

                                if (currentToken.Type != Tokenizer2.TokenType.EOL && currentToken.Type != Tokenizer2.TokenType.EOF)
                                    return false;

                                instructions.Add(currentInstruction);
                                state = State.Ident;
                            }
                            else
                                return false; // Syntax error! 
                        }
                        else
                        {
                            Label label;

                            if (!tables.GetLabelByName(currentToken.Lexeme, out label))
                                return false;

                            currentInstruction.Arguments.Add(label.Index.ToString());

                            currentToken = tokenizer.GetNextToken();
                        }
                    }
                    else if (currentToken.Type == Tokenizer2.TokenType.CloseParent)
                    {
                        currentToken = tokenizer.GetNextToken();

                        if (currentToken.Type != Tokenizer2.TokenType.EOL && currentToken.Type != Tokenizer2.TokenType.EOF)
                            return false; // Syntax error! 

                        instructions.Add(currentInstruction);

                        state = State.Ident;
                    }
                    else
                    {
                        return false; // Syntax error!
                    }
                    break;
            }

            SkipEOL(); // Skips End of Lines

            currentToken = tokenizer.GetCurrentToken();
        }

        return true;
    }

    void SkipEOL()
    {
        while (tokenizer.GetCurrentToken().Type == Tokenizer2.TokenType.EOL)
            tokenizer.GetNextToken();
    }
}
